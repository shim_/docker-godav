#!/bin/bash

genpasswd() {
   local l=$1
   [ "$l" == "" ] && l=16
   tr -dc A-Za-z0-9_ < /dev/urandom | head -c ${l} | xargs
}


USERS=$(echo $1 | tr "," "\n")
if [ "$DAVUSERS" != "" ]; then
   USERS=$(echo $DAVUSERS | tr "," "\n")
fi
if [ "$USERS" == "" ]; then
   USERS=( "admin:random:rw:/" )
fi
rm -f /etc/webdav/random
for CRED in $USERS; do
    CONF=($(echo $CRED | tr ":" "\n"))
    USER=${CONF[0]}
    PASS=${CONF[1]}
    MODIFY="false"
    if [ "$PASS" == "random" ]; then
       PASS="$(genpasswd 12)"
       echo "A password for User: $USER has been generated"
       echo "Password: $PASS" 
       echo "$USER:$PASS" >> /etc/webdav/random
    fi
    if [ "${CONF[2]}" == "rw" ]; then
       MODIFY="true"     
    fi
    SCOPE="/"
    if [ "${#CONF[@]}" == "4" ]; then
       SCOPE=${CONF[3]}
       NODE="$NODE    scope: /fakeroot/$SCOPE\n"
    fi
    NODE="  - username: '$USER'\n"
    NODE="$NODE    password: '$PASS'\n"
    NODE="$NODE    modify: $MODIFY\n"
    printf "$NODE" >> /etc/webdav/config.yaml
done
if [ ! -z "$DAV_ROOT" ]; then
 CONF="$(tail -n+2 /etc/webdav/config.yaml)"
 FAKE="/fakeroot/$DAV_ROOT"
 mkdir -p ${FAKE%/*}
 ln -s /davroot "$FAKE" 
 echo "scope: /fakeroot" > /etc/webdav/config.yaml
 echo "$CONF" >> /etc/webdav/config.yaml
fi
webdav
